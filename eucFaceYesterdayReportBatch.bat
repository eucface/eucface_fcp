xcopy \\fcp1\csv c:\Data\FCP\ /d /y
xcopy \\fcp2\csv c:\Data\FCP\ /d /y
xcopy \\fcp3\csv c:\Data\FCP\ /d /y
xcopy \\fcp4\csv c:\Data\FCP\ /d /y
xcopy \\fcp5\csv c:\Data\FCP\ /d /y
xcopy \\fcp6\csv c:\Data\FCP\ /d /y

Rscript -e "require ('knitr'); knit2html('c:/Scripts/eucface_fcp/eucFACEhtmlReport.Rmd', paste0('c:/Data/FCP/reports/','EucFACE-',Sys.Date()-1))"
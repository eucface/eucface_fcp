EucFACE Daily Report 
========================================================



```
## Report of EucFACE Facility Performance on 2016-02-29
```

```
## Created on: 2016-03-01 14:27:15
```





Wind Rose plots for the six rings, control rings harvest windspeed from their paired fumigated ring and therefore the wind data presented is the 30 second intergral of windspeed and direction

![plot of chunk windRosePlots](c:/Data/FCP/figures/2016-02-29/windRosePlots-1.png)

windgraph1
![plot of chunk windgraph1](c:/Data/FCP/figures/2016-02-29/windgraph1-1.png)

![plot of chunk windgraph2](c:/Data/FCP/figures/2016-02-29/windgraph2-1.png)




![plot of chunk windgraph3](c:/Data/FCP/figures/2016-02-29/windgraph3-1.png)

Density plots for the fumigation system as a whole, and independentaly at each ring.  One minute concentration band is 10% from target, Absolute is PPM of CO2 away from setpoint, the band is 50% of the treatment.  Grab concentrations band is 20% of target.
![plot of chunk densityFCPPlots](c:/Data/FCP/figures/2016-02-29/densityFCPPlots-1.png)

15 minute tables for the fumigated Rings
<table>
 <thead>
  <tr>
   <th style="text-align:left;"> P </th>
   <th style="text-align:left;"> Quater </th>
   <th style="text-align:left;"> DATE </th>
   <th style="text-align:left;"> WS </th>
   <th style="text-align:left;"> TAIR </th>
   <th style="text-align:left;"> PVR </th>
   <th style="text-align:left;"> PVC </th>
   <th style="text-align:left;"> CBASE </th>
   <th style="text-align:left;"> CSET </th>
   <th style="text-align:left;"> C1MIN </th>
   <th style="text-align:left;"> Absolute </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 05:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.71 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 57 </td>
   <td style="text-align:left;"> 61 </td>
   <td style="text-align:left;"> 477 </td>
   <td style="text-align:left;"> 627 </td>
   <td style="text-align:left;"> 650 </td>
   <td style="text-align:left;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 05:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.79 </td>
   <td style="text-align:left;"> 16 </td>
   <td style="text-align:left;"> 58 </td>
   <td style="text-align:left;"> 55 </td>
   <td style="text-align:left;"> 472 </td>
   <td style="text-align:left;"> 622 </td>
   <td style="text-align:left;"> 620 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 06:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.74 </td>
   <td style="text-align:left;"> 16 </td>
   <td style="text-align:left;"> 65 </td>
   <td style="text-align:left;"> 62 </td>
   <td style="text-align:left;"> 489 </td>
   <td style="text-align:left;"> 639 </td>
   <td style="text-align:left;"> 638 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 06:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.57 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 76 </td>
   <td style="text-align:left;"> 78 </td>
   <td style="text-align:left;"> 506 </td>
   <td style="text-align:left;"> 656 </td>
   <td style="text-align:left;"> 653 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 06:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.61 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 53 </td>
   <td style="text-align:left;"> 50 </td>
   <td style="text-align:left;"> 477 </td>
   <td style="text-align:left;"> 627 </td>
   <td style="text-align:left;"> 638 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 06:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.46 </td>
   <td style="text-align:left;"> 18 </td>
   <td style="text-align:left;"> 64 </td>
   <td style="text-align:left;"> 65 </td>
   <td style="text-align:left;"> 503 </td>
   <td style="text-align:left;"> 653 </td>
   <td style="text-align:left;"> 646 </td>
   <td style="text-align:left;"> -7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 07:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.31 </td>
   <td style="text-align:left;"> 20 </td>
   <td style="text-align:left;"> 130 </td>
   <td style="text-align:left;"> 134 </td>
   <td style="text-align:left;"> 514 </td>
   <td style="text-align:left;"> 664 </td>
   <td style="text-align:left;"> 634 </td>
   <td style="text-align:left;"> -30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 07:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.43 </td>
   <td style="text-align:left;"> 20 </td>
   <td style="text-align:left;"> 124 </td>
   <td style="text-align:left;"> 115 </td>
   <td style="text-align:left;"> 456 </td>
   <td style="text-align:left;"> 606 </td>
   <td style="text-align:left;"> 610 </td>
   <td style="text-align:left;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 07:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.47 </td>
   <td style="text-align:left;"> 21 </td>
   <td style="text-align:left;"> 193 </td>
   <td style="text-align:left;"> 196 </td>
   <td style="text-align:left;"> 451 </td>
   <td style="text-align:left;"> 601 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> -39 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 07:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.30 </td>
   <td style="text-align:left;"> 22 </td>
   <td style="text-align:left;"> 354 </td>
   <td style="text-align:left;"> 352 </td>
   <td style="text-align:left;"> 443 </td>
   <td style="text-align:left;"> 593 </td>
   <td style="text-align:left;"> 531 </td>
   <td style="text-align:left;"> -62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 08:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.30 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 254 </td>
   <td style="text-align:left;"> 270 </td>
   <td style="text-align:left;"> 435 </td>
   <td style="text-align:left;"> 585 </td>
   <td style="text-align:left;"> 606 </td>
   <td style="text-align:left;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 08:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.51 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 292 </td>
   <td style="text-align:left;"> 285 </td>
   <td style="text-align:left;"> 429 </td>
   <td style="text-align:left;"> 579 </td>
   <td style="text-align:left;"> 567 </td>
   <td style="text-align:left;"> -12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 08:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.88 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 291 </td>
   <td style="text-align:left;"> 288 </td>
   <td style="text-align:left;"> 418 </td>
   <td style="text-align:left;"> 568 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> -5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 08:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.64 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 326 </td>
   <td style="text-align:left;"> 328 </td>
   <td style="text-align:left;"> 409 </td>
   <td style="text-align:left;"> 559 </td>
   <td style="text-align:left;"> 529 </td>
   <td style="text-align:left;"> -30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 09:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.42 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 500 </td>
   <td style="text-align:left;"> 494 </td>
   <td style="text-align:left;"> 403 </td>
   <td style="text-align:left;"> 553 </td>
   <td style="text-align:left;"> 497 </td>
   <td style="text-align:left;"> -55 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 09:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.57 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 381 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 574 </td>
   <td style="text-align:left;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 09:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.95 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 426 </td>
   <td style="text-align:left;"> 434 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 540 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 09:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.41 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 328 </td>
   <td style="text-align:left;"> 333 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 571 </td>
   <td style="text-align:left;"> 28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 10:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.71 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 279 </td>
   <td style="text-align:left;"> 278 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 539 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 10:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.80 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 237 </td>
   <td style="text-align:left;"> 235 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 575 </td>
   <td style="text-align:left;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 10:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.59 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 247 </td>
   <td style="text-align:left;"> 259 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 519 </td>
   <td style="text-align:left;"> -23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 10:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.09 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 294 </td>
   <td style="text-align:left;"> 290 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 522 </td>
   <td style="text-align:left;"> -20 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 11:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.25 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 311 </td>
   <td style="text-align:left;"> 313 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 11:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.08 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 279 </td>
   <td style="text-align:left;"> 280 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 522 </td>
   <td style="text-align:left;"> -21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 11:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.79 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 293 </td>
   <td style="text-align:left;"> 293 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 550 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 11:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.22 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 278 </td>
   <td style="text-align:left;"> 279 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 12:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.10 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 271 </td>
   <td style="text-align:left;"> 272 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 12:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.21 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 236 </td>
   <td style="text-align:left;"> 232 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 577 </td>
   <td style="text-align:left;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 12:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.48 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 254 </td>
   <td style="text-align:left;"> 256 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 12:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.15 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 306 </td>
   <td style="text-align:left;"> 308 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 522 </td>
   <td style="text-align:left;"> -23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 13:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.75 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 405 </td>
   <td style="text-align:left;"> 405 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 538 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 13:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.38 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 311 </td>
   <td style="text-align:left;"> 308 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 561 </td>
   <td style="text-align:left;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 13:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.15 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 390 </td>
   <td style="text-align:left;"> 388 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 533 </td>
   <td style="text-align:left;"> -14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 13:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.41 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 355 </td>
   <td style="text-align:left;"> 354 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 564 </td>
   <td style="text-align:left;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 14:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.43 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 536 </td>
   <td style="text-align:left;"> -9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 14:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.08 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 442 </td>
   <td style="text-align:left;"> 445 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 14:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.43 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 440 </td>
   <td style="text-align:left;"> 447 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 557 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 14:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.21 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 565 </td>
   <td style="text-align:left;"> 571 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 524 </td>
   <td style="text-align:left;"> -21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 15:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.47 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 612 </td>
   <td style="text-align:left;"> 611 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 15:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.78 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 580 </td>
   <td style="text-align:left;"> 584 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 538 </td>
   <td style="text-align:left;"> -6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 15:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.76 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 525 </td>
   <td style="text-align:left;"> 528 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 557 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 15:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.80 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 494 </td>
   <td style="text-align:left;"> 494 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 569 </td>
   <td style="text-align:left;"> 21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 16:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.12 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 522 </td>
   <td style="text-align:left;"> 521 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 16:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.07 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 530 </td>
   <td style="text-align:left;"> 526 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 540 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 16:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.15 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 443 </td>
   <td style="text-align:left;"> 445 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 557 </td>
   <td style="text-align:left;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 16:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.30 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 487 </td>
   <td style="text-align:left;"> 483 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 17:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.93 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 438 </td>
   <td style="text-align:left;"> 438 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 556 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 17:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.49 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 444 </td>
   <td style="text-align:left;"> 445 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> -4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 17:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.43 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 476 </td>
   <td style="text-align:left;"> 473 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 17:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.87 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 240 </td>
   <td style="text-align:left;"> 240 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 583 </td>
   <td style="text-align:left;"> 38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 18:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.83 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 217 </td>
   <td style="text-align:left;"> 216 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 18:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.72 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 223 </td>
   <td style="text-align:left;"> 221 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 555 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 18:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.78 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 173 </td>
   <td style="text-align:left;"> 172 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> 17 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 18:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.91 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 161 </td>
   <td style="text-align:left;"> 168 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 1 </td>
   <td style="text-align:left;"> 19:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.32 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 192 </td>
   <td style="text-align:left;"> 177 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 4 </td>
  </tr>
</tbody>
</table>
<table>
 <thead>
  <tr>
   <th style="text-align:left;"> P </th>
   <th style="text-align:left;"> Quater </th>
   <th style="text-align:left;"> DATE </th>
   <th style="text-align:left;"> WS </th>
   <th style="text-align:left;"> TAIR </th>
   <th style="text-align:left;"> PVR </th>
   <th style="text-align:left;"> PVC </th>
   <th style="text-align:left;"> CBASE </th>
   <th style="text-align:left;"> CSET </th>
   <th style="text-align:left;"> C1MIN </th>
   <th style="text-align:left;"> Absolute </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 05:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.24 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 46 </td>
   <td style="text-align:left;"> 42 </td>
   <td style="text-align:left;"> 477 </td>
   <td style="text-align:left;"> 628 </td>
   <td style="text-align:left;"> 643 </td>
   <td style="text-align:left;"> 15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 05:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.98 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 31 </td>
   <td style="text-align:left;"> 39 </td>
   <td style="text-align:left;"> 473 </td>
   <td style="text-align:left;"> 623 </td>
   <td style="text-align:left;"> 618 </td>
   <td style="text-align:left;"> -4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 06:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.97 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 52 </td>
   <td style="text-align:left;"> 48 </td>
   <td style="text-align:left;"> 489 </td>
   <td style="text-align:left;"> 639 </td>
   <td style="text-align:left;"> 644 </td>
   <td style="text-align:left;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 06:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.69 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 34 </td>
   <td style="text-align:left;"> 38 </td>
   <td style="text-align:left;"> 507 </td>
   <td style="text-align:left;"> 657 </td>
   <td style="text-align:left;"> 654 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 06:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.60 </td>
   <td style="text-align:left;"> 18 </td>
   <td style="text-align:left;"> 41 </td>
   <td style="text-align:left;"> 42 </td>
   <td style="text-align:left;"> 478 </td>
   <td style="text-align:left;"> 628 </td>
   <td style="text-align:left;"> 628 </td>
   <td style="text-align:left;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 06:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.41 </td>
   <td style="text-align:left;"> 18 </td>
   <td style="text-align:left;"> 85 </td>
   <td style="text-align:left;"> 85 </td>
   <td style="text-align:left;"> 503 </td>
   <td style="text-align:left;"> 653 </td>
   <td style="text-align:left;"> 620 </td>
   <td style="text-align:left;"> -33 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 07:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.33 </td>
   <td style="text-align:left;"> 19 </td>
   <td style="text-align:left;"> 117 </td>
   <td style="text-align:left;"> 117 </td>
   <td style="text-align:left;"> 512 </td>
   <td style="text-align:left;"> 662 </td>
   <td style="text-align:left;"> 675 </td>
   <td style="text-align:left;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 07:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.46 </td>
   <td style="text-align:left;"> 20 </td>
   <td style="text-align:left;"> 113 </td>
   <td style="text-align:left;"> 110 </td>
   <td style="text-align:left;"> 457 </td>
   <td style="text-align:left;"> 607 </td>
   <td style="text-align:left;"> 592 </td>
   <td style="text-align:left;"> -15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 07:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.38 </td>
   <td style="text-align:left;"> 21 </td>
   <td style="text-align:left;"> 180 </td>
   <td style="text-align:left;"> 199 </td>
   <td style="text-align:left;"> 451 </td>
   <td style="text-align:left;"> 601 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> -38 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 07:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.45 </td>
   <td style="text-align:left;"> 21 </td>
   <td style="text-align:left;"> 245 </td>
   <td style="text-align:left;"> 243 </td>
   <td style="text-align:left;"> 443 </td>
   <td style="text-align:left;"> 593 </td>
   <td style="text-align:left;"> 573 </td>
   <td style="text-align:left;"> -19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 08:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.57 </td>
   <td style="text-align:left;"> 22 </td>
   <td style="text-align:left;"> 247 </td>
   <td style="text-align:left;"> 245 </td>
   <td style="text-align:left;"> 434 </td>
   <td style="text-align:left;"> 584 </td>
   <td style="text-align:left;"> 581 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 08:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.74 </td>
   <td style="text-align:left;"> 22 </td>
   <td style="text-align:left;"> 232 </td>
   <td style="text-align:left;"> 227 </td>
   <td style="text-align:left;"> 428 </td>
   <td style="text-align:left;"> 578 </td>
   <td style="text-align:left;"> 559 </td>
   <td style="text-align:left;"> -19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 08:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.94 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 267 </td>
   <td style="text-align:left;"> 269 </td>
   <td style="text-align:left;"> 417 </td>
   <td style="text-align:left;"> 567 </td>
   <td style="text-align:left;"> 559 </td>
   <td style="text-align:left;"> -8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 08:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.08 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 271 </td>
   <td style="text-align:left;"> 286 </td>
   <td style="text-align:left;"> 409 </td>
   <td style="text-align:left;"> 559 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> -18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 09:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.21 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 310 </td>
   <td style="text-align:left;"> 306 </td>
   <td style="text-align:left;"> 403 </td>
   <td style="text-align:left;"> 553 </td>
   <td style="text-align:left;"> 551 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 09:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.21 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 292 </td>
   <td style="text-align:left;"> 293 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 551 </td>
   <td style="text-align:left;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 09:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.29 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 311 </td>
   <td style="text-align:left;"> 311 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 536 </td>
   <td style="text-align:left;"> -6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 09:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.73 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 288 </td>
   <td style="text-align:left;"> 295 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 553 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 10:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.14 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 281 </td>
   <td style="text-align:left;"> 279 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 537 </td>
   <td style="text-align:left;"> -5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 10:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.09 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 377 </td>
   <td style="text-align:left;"> 375 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 502 </td>
   <td style="text-align:left;"> -40 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 10:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.05 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 358 </td>
   <td style="text-align:left;"> 364 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 528 </td>
   <td style="text-align:left;"> -14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 10:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.36 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 352 </td>
   <td style="text-align:left;"> 346 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 11:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.85 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 436 </td>
   <td style="text-align:left;"> 442 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 510 </td>
   <td style="text-align:left;"> -31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 11:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.43 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 335 </td>
   <td style="text-align:left;"> 333 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 568 </td>
   <td style="text-align:left;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 11:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.13 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 252 </td>
   <td style="text-align:left;"> 259 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 578 </td>
   <td style="text-align:left;"> 35 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 11:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.87 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 236 </td>
   <td style="text-align:left;"> 237 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 506 </td>
   <td style="text-align:left;"> -34 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 12:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.94 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 366 </td>
   <td style="text-align:left;"> 363 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 538 </td>
   <td style="text-align:left;"> -7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 12:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.49 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 270 </td>
   <td style="text-align:left;"> 268 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 554 </td>
   <td style="text-align:left;"> 9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 12:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.59 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 195 </td>
   <td style="text-align:left;"> 197 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 12:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.52 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 245 </td>
   <td style="text-align:left;"> 255 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 521 </td>
   <td style="text-align:left;"> -25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 13:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.17 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 348 </td>
   <td style="text-align:left;"> 337 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 537 </td>
   <td style="text-align:left;"> -4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 13:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.88 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 248 </td>
   <td style="text-align:left;"> 249 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 563 </td>
   <td style="text-align:left;"> 19 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 13:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.53 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 289 </td>
   <td style="text-align:left;"> 293 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 13:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.99 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 232 </td>
   <td style="text-align:left;"> 227 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 560 </td>
   <td style="text-align:left;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 14:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.84 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 299 </td>
   <td style="text-align:left;"> 311 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 535 </td>
   <td style="text-align:left;"> -10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 14:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.91 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 377 </td>
   <td style="text-align:left;"> 390 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 537 </td>
   <td style="text-align:left;"> -11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 14:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.69 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 376 </td>
   <td style="text-align:left;"> 377 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 14:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 5.09 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 488 </td>
   <td style="text-align:left;"> 498 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 532 </td>
   <td style="text-align:left;"> -13 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 15:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.93 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 442 </td>
   <td style="text-align:left;"> 446 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 15:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.68 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 498 </td>
   <td style="text-align:left;"> 500 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 538 </td>
   <td style="text-align:left;"> -6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 15:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.10 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 462 </td>
   <td style="text-align:left;"> 467 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 15:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.11 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 528 </td>
   <td style="text-align:left;"> 520 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 16:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.47 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 439 </td>
   <td style="text-align:left;"> 425 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 560 </td>
   <td style="text-align:left;"> 14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 16:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.91 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 367 </td>
   <td style="text-align:left;"> 379 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 16:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.25 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 365 </td>
   <td style="text-align:left;"> 368 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 16:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.26 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 17:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 4.34 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 354 </td>
   <td style="text-align:left;"> 358 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 555 </td>
   <td style="text-align:left;"> 5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 17:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.13 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 320 </td>
   <td style="text-align:left;"> 319 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 562 </td>
   <td style="text-align:left;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 17:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.89 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 326 </td>
   <td style="text-align:left;"> 328 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 550 </td>
   <td style="text-align:left;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 17:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.07 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 241 </td>
   <td style="text-align:left;"> 249 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 18:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.47 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 208 </td>
   <td style="text-align:left;"> 209 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 554 </td>
   <td style="text-align:left;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 18:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.05 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 269 </td>
   <td style="text-align:left;"> 258 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 0 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 18:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.78 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 217 </td>
   <td style="text-align:left;"> 216 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 18:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.63 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 195 </td>
   <td style="text-align:left;"> 190 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 557 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 4 </td>
   <td style="text-align:left;"> 19:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.73 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 158 </td>
   <td style="text-align:left;"> 176 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
</tbody>
</table>
<table>
 <thead>
  <tr>
   <th style="text-align:left;"> P </th>
   <th style="text-align:left;"> Quater </th>
   <th style="text-align:left;"> DATE </th>
   <th style="text-align:left;"> WS </th>
   <th style="text-align:left;"> TAIR </th>
   <th style="text-align:left;"> PVR </th>
   <th style="text-align:left;"> PVC </th>
   <th style="text-align:left;"> CBASE </th>
   <th style="text-align:left;"> CSET </th>
   <th style="text-align:left;"> C1MIN </th>
   <th style="text-align:left;"> Absolute </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 05:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.92 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 79 </td>
   <td style="text-align:left;"> 79 </td>
   <td style="text-align:left;"> 478 </td>
   <td style="text-align:left;"> 628 </td>
   <td style="text-align:left;"> 778 </td>
   <td style="text-align:left;"> 150 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 05:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.57 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 59 </td>
   <td style="text-align:left;"> 62 </td>
   <td style="text-align:left;"> 473 </td>
   <td style="text-align:left;"> 623 </td>
   <td style="text-align:left;"> 610 </td>
   <td style="text-align:left;"> -12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 06:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.45 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 81 </td>
   <td style="text-align:left;"> 80 </td>
   <td style="text-align:left;"> 491 </td>
   <td style="text-align:left;"> 641 </td>
   <td style="text-align:left;"> 629 </td>
   <td style="text-align:left;"> -11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 06:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.52 </td>
   <td style="text-align:left;"> 17 </td>
   <td style="text-align:left;"> 78 </td>
   <td style="text-align:left;"> 81 </td>
   <td style="text-align:left;"> 507 </td>
   <td style="text-align:left;"> 657 </td>
   <td style="text-align:left;"> 669 </td>
   <td style="text-align:left;"> 11 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 06:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.53 </td>
   <td style="text-align:left;"> 18 </td>
   <td style="text-align:left;"> 52 </td>
   <td style="text-align:left;"> 55 </td>
   <td style="text-align:left;"> 474 </td>
   <td style="text-align:left;"> 624 </td>
   <td style="text-align:left;"> 633 </td>
   <td style="text-align:left;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 06:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.59 </td>
   <td style="text-align:left;"> 18 </td>
   <td style="text-align:left;"> 39 </td>
   <td style="text-align:left;"> 41 </td>
   <td style="text-align:left;"> 505 </td>
   <td style="text-align:left;"> 655 </td>
   <td style="text-align:left;"> 654 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 07:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.41 </td>
   <td style="text-align:left;"> 20 </td>
   <td style="text-align:left;"> 96 </td>
   <td style="text-align:left;"> 93 </td>
   <td style="text-align:left;"> 510 </td>
   <td style="text-align:left;"> 660 </td>
   <td style="text-align:left;"> 637 </td>
   <td style="text-align:left;"> -23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 07:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.49 </td>
   <td style="text-align:left;"> 20 </td>
   <td style="text-align:left;"> 81 </td>
   <td style="text-align:left;"> 92 </td>
   <td style="text-align:left;"> 456 </td>
   <td style="text-align:left;"> 606 </td>
   <td style="text-align:left;"> 603 </td>
   <td style="text-align:left;"> -2 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 07:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.40 </td>
   <td style="text-align:left;"> 21 </td>
   <td style="text-align:left;"> 158 </td>
   <td style="text-align:left;"> 158 </td>
   <td style="text-align:left;"> 451 </td>
   <td style="text-align:left;"> 601 </td>
   <td style="text-align:left;"> 560 </td>
   <td style="text-align:left;"> -40 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 07:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.33 </td>
   <td style="text-align:left;"> 22 </td>
   <td style="text-align:left;"> 263 </td>
   <td style="text-align:left;"> 268 </td>
   <td style="text-align:left;"> 442 </td>
   <td style="text-align:left;"> 592 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> -51 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 08:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.27 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 378 </td>
   <td style="text-align:left;"> 380 </td>
   <td style="text-align:left;"> 434 </td>
   <td style="text-align:left;"> 583 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> -34 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 08:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.73 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 290 </td>
   <td style="text-align:left;"> 286 </td>
   <td style="text-align:left;"> 428 </td>
   <td style="text-align:left;"> 578 </td>
   <td style="text-align:left;"> 592 </td>
   <td style="text-align:left;"> 13 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 08:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.84 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 272 </td>
   <td style="text-align:left;"> 276 </td>
   <td style="text-align:left;"> 416 </td>
   <td style="text-align:left;"> 566 </td>
   <td style="text-align:left;"> 557 </td>
   <td style="text-align:left;"> -9 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 08:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.68 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 305 </td>
   <td style="text-align:left;"> 310 </td>
   <td style="text-align:left;"> 409 </td>
   <td style="text-align:left;"> 559 </td>
   <td style="text-align:left;"> 536 </td>
   <td style="text-align:left;"> -23 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 09:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.80 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 314 </td>
   <td style="text-align:left;"> 312 </td>
   <td style="text-align:left;"> 401 </td>
   <td style="text-align:left;"> 551 </td>
   <td style="text-align:left;"> 564 </td>
   <td style="text-align:left;"> 12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 09:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.04 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 344 </td>
   <td style="text-align:left;"> 348 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 519 </td>
   <td style="text-align:left;"> -27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 09:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.30 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 334 </td>
   <td style="text-align:left;"> 337 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 09:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.70 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 318 </td>
   <td style="text-align:left;"> 304 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 551 </td>
   <td style="text-align:left;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 10:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.08 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 228 </td>
   <td style="text-align:left;"> 227 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 575 </td>
   <td style="text-align:left;"> 32 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 10:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.08 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 318 </td>
   <td style="text-align:left;"> 323 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 517 </td>
   <td style="text-align:left;"> -25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 10:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.75 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 343 </td>
   <td style="text-align:left;"> 353 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 517 </td>
   <td style="text-align:left;"> -25 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 10:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.54 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 364 </td>
   <td style="text-align:left;"> 366 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 532 </td>
   <td style="text-align:left;"> -10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 11:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.73 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 389 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 511 </td>
   <td style="text-align:left;"> -31 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 11:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.09 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 11:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.17 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 259 </td>
   <td style="text-align:left;"> 266 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 632 </td>
   <td style="text-align:left;"> 89 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 11:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 0.91 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 213 </td>
   <td style="text-align:left;"> 217 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 516 </td>
   <td style="text-align:left;"> -24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 12:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.09 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 298 </td>
   <td style="text-align:left;"> 299 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 540 </td>
   <td style="text-align:left;"> -4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 12:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.02 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 253 </td>
   <td style="text-align:left;"> 254 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 570 </td>
   <td style="text-align:left;"> 26 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 12:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.60 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 184 </td>
   <td style="text-align:left;"> 185 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 12:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.58 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 246 </td>
   <td style="text-align:left;"> 247 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 517 </td>
   <td style="text-align:left;"> -28 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 13:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.71 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 365 </td>
   <td style="text-align:left;"> 370 </td>
   <td style="text-align:left;"> 392 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 525 </td>
   <td style="text-align:left;"> -16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 13:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.27 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 249 </td>
   <td style="text-align:left;"> 249 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 575 </td>
   <td style="text-align:left;"> 30 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 13:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.24 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 334 </td>
   <td style="text-align:left;"> 330 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> -5 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 13:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.71 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 254 </td>
   <td style="text-align:left;"> 253 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 549 </td>
   <td style="text-align:left;"> 573 </td>
   <td style="text-align:left;"> 24 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 14:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.09 </td>
   <td style="text-align:left;"> 27 </td>
   <td style="text-align:left;"> 281 </td>
   <td style="text-align:left;"> 286 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 533 </td>
   <td style="text-align:left;"> -12 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 14:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.06 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 352 </td>
   <td style="text-align:left;"> 348 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> -4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 14:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.33 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 402 </td>
   <td style="text-align:left;"> 399 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 538 </td>
   <td style="text-align:left;"> -6 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 14:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.38 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 477 </td>
   <td style="text-align:left;"> 475 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 528 </td>
   <td style="text-align:left;"> -15 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 15:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.70 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 433 </td>
   <td style="text-align:left;"> 432 </td>
   <td style="text-align:left;"> 396 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 15:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.18 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 448 </td>
   <td style="text-align:left;"> 447 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 15:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.54 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 455 </td>
   <td style="text-align:left;"> 453 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 15:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.92 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 468 </td>
   <td style="text-align:left;"> 474 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> -1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 16:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.22 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 372 </td>
   <td style="text-align:left;"> 371 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 572 </td>
   <td style="text-align:left;"> 27 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 16:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.57 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 354 </td>
   <td style="text-align:left;"> 357 </td>
   <td style="text-align:left;"> 391 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> 542 </td>
   <td style="text-align:left;"> 1 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 16:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.43 </td>
   <td style="text-align:left;"> 26 </td>
   <td style="text-align:left;"> 372 </td>
   <td style="text-align:left;"> 380 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 541 </td>
   <td style="text-align:left;"> -3 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 16:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.95 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 414 </td>
   <td style="text-align:left;"> 408 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 530 </td>
   <td style="text-align:left;"> -14 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 17:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.34 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 387 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 4 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 17:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 3.01 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 378 </td>
   <td style="text-align:left;"> 378 </td>
   <td style="text-align:left;"> 398 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 565 </td>
   <td style="text-align:left;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 17:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.93 </td>
   <td style="text-align:left;"> 25 </td>
   <td style="text-align:left;"> 331 </td>
   <td style="text-align:left;"> 326 </td>
   <td style="text-align:left;"> 393 </td>
   <td style="text-align:left;"> 543 </td>
   <td style="text-align:left;"> 551 </td>
   <td style="text-align:left;"> 8 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 17:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.85 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 259 </td>
   <td style="text-align:left;"> 264 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 561 </td>
   <td style="text-align:left;"> 16 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 18:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.25 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 191 </td>
   <td style="text-align:left;"> 192 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 547 </td>
   <td style="text-align:left;"> 569 </td>
   <td style="text-align:left;"> 22 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 18:15 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 2.42 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 219 </td>
   <td style="text-align:left;"> 217 </td>
   <td style="text-align:left;"> 394 </td>
   <td style="text-align:left;"> 544 </td>
   <td style="text-align:left;"> 552 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 18:30 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.56 </td>
   <td style="text-align:left;"> 24 </td>
   <td style="text-align:left;"> 150 </td>
   <td style="text-align:left;"> 147 </td>
   <td style="text-align:left;"> 395 </td>
   <td style="text-align:left;"> 545 </td>
   <td style="text-align:left;"> 556 </td>
   <td style="text-align:left;"> 10 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 18:45 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.70 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 134 </td>
   <td style="text-align:left;"> 134 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 546 </td>
   <td style="text-align:left;"> 554 </td>
   <td style="text-align:left;"> 7 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> 5 </td>
   <td style="text-align:left;"> 19:00 </td>
   <td style="text-align:left;"> 2016-02-29 </td>
   <td style="text-align:left;"> 1.17 </td>
   <td style="text-align:left;"> 23 </td>
   <td style="text-align:left;"> 166 </td>
   <td style="text-align:left;"> 162 </td>
   <td style="text-align:left;"> 397 </td>
   <td style="text-align:left;"> 548 </td>
   <td style="text-align:left;"> 524 </td>
   <td style="text-align:left;"> -23 </td>
  </tr>
</tbody>
</table>

Four hour CO2 concentrations of all six IRGAS when FACE system is off, used to diagnose when to calibrate IRGAs.
![plot of chunk nighttimeCO2plot](c:/Data/FCP/figures/2016-02-29/nighttimeCO2plot-1.png)

One minute CO2 concentrations PPM away from target in the fumigated rings.  Blue band represents the +-50% of treatment, green represents the +-25% of treatment

```
## [[1]]
```

![plot of chunk daytimeCO2plot](c:/Data/FCP/figures/2016-02-29/daytimeCO2plot-1.png)

```
## 
## [[2]]
```

![plot of chunk daytimeCO2plot](c:/Data/FCP/figures/2016-02-29/daytimeCO2plot-2.png)

```
## 
## [[3]]
```

![plot of chunk daytimeCO2plot](c:/Data/FCP/figures/2016-02-29/daytimeCO2plot-3.png)


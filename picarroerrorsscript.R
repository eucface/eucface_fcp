badFile<-read.csv("CFIDS2074-20140527-111333Z-DataLog_User.dat", header=TRUE, strip.white=TRUE, sep="")

badpicarroDataFrame<-subset(picarroDataFrame,
                            DateTime<=as.POSIXct("2014-05-27 11:20:00")|
                              DateTime>=as.POSIXct("2014-05-27 11:27:00"))

picarroDataFrame[picarroDataFrame == -20000] <- NA

picarroDataFrame<-picarroDataFrame[complete.cases(picarroDataFrame),]

picarroDeltaCH4<-summaryBy(formula=HP_Delta_iCH4_5min+
                             HR_Delta_iCH4_5min~timeCuts, 
                           data=picarroDataFrame, 
                           FUN=c(mean,sd),  na.rm=TRUE)


picarro12CO2<-summaryBy(formula=X12CO2_dry+Delta_5min_iCO2~timeCuts, 
                        data=picarroDataFrame, 
                        FUN=c(mean,sd), na.rm=TRUE)

picarroCH4dry<-summaryBy(formula=HR_12CH4_dry+HR_13CH4~timeCuts,
                         data=picarroDataFrame,
                         FUN=c(mean,sd), na.rm=TRUE)

picarroDeltaCH4Hour<-summaryBy(formula=HP_Delta_iCH4_5min+
                                 HR_Delta_iCH4_5min~timeCuts, 
                               data=picarroDataFrameHour, 
                               FUN=c(mean,sd),  na.rm=TRUE)


picarro12CO2Hour<-summaryBy(formula=X12CO2_dry+Delta_5min_iCO2~timeCuts, 
                            data=picarroDataFrameHour, 
                            FUN=c(mean,sd), na.rm=TRUE)

picarroCH4dryHour<-summaryBy(formula=HR_12CH4_dry+HR_13CH4~timeCuts,
                             data=picarroDataFrameHour,
                             FUN=c(mean,sd), na.rm=TRUE)



picarroTemp<-summaryBy(formula=DasTemp~timeCuts, 
                       data=picarroDataFrame, 
                       FUN=c(mean,sd),  na.rm=TRUE)

ddeltaCH4HP <- ggplot(picarroDataFrameByTimeCuts, aes(x=as.POSIXct(timeCuts), 
                                                      y=Delta_Raw_iCO2_mean))+
  geom_errorbar(aes(ymin=Delta_Raw_iCO2_mean-Delta_Raw_iCO2_sd, 
                    ymax=Delta_Raw_iCO2_mean+Delta_Raw_iCO2_sd), 
                colour="red", width=10)+
  geom_point()+
  geom_line()


deltaCH4HR <- ggplot(picarroDelta, aes(x=as.POSIXct(timeCuts), 
                                       y=HR_Delta_iCH4_5min.mean))+
  geom_errorbar(aes(ymin=HR_Delta_iCH4_5min.mean-HR_Delta_iCH4_5min.sd, 
                    ymax=HR_Delta_iCH4_5min.mean+HR_Delta_iCH4_5min.sd), 
                colour="red", width=.1)+
  geom_point()+
  geom_line()

delta12CO2 <-ggplot(picarroDelta, aes(x=as.POSIXct(timeCuts), 
                                      y=Delta_5min_iCO2.mean))+
  geom_errorbar(aes(ymin=Delta_5min_iCO2.mean-Delta_5min_iCO2.sd, 
                    ymax=Delta_5min_iCO2.mean+Delta_5min_iCO2.sd), 
                colour="red", width=.1)+
  geom_point()+
  geom_line()

temp <-ggplot(picarroTemp, aes(x=as.POSIXct(timeCuts), 
                               y=DasTemp.mean))+
  geom_errorbar(aes(ymin=DasTemp.mean-DasTemp.sd, 
                    ymax=DasTemp.mean+DasTemp.sd), 
                colour="red", width=.1)+
  geom_point()+
  geom_line()

temps<-summaryBy(formula=Celsius..C.+Humidity..rh.~timeCuts, 
                 data=tempData, 
                 FUN=c(mean,sd),  na.rm=TRUE)

tempRoom <-ggplot(temps, aes(x=as.POSIXct(timeCuts), 
                             y=CelsiusC.mean))+
  geom_errorbar(aes(ymin=CelsiusC.mean-CelsiusC.sd, 
                    ymax=CelsiusC.mean+CelsiusC.sd), 
                colour="red", width=.1)+
  geom_point()+
  geom_line()

library(HIEv)
